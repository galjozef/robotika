function createButtons(robot, referenceX, squareSize, sedmicka)
    % creates gui to control robot
    lastPlaced = " ";
    nRows = 10;
    nColumns = 7;
    dispSize = get(groot, 'ScreenSize');
    sirka = dispSize(3);
    vyska = dispSize(4);
    figSize = [sirka / 3, vyska - 100];
    btnSz = figSize(1) / 9;
    fig = figure('Color', 'black', 'pos', [(sirka - figSize(1)) / 2, (vyska - figSize(2)) / 2, figSize(1), figSize(2) - 100]);
    set(fig, 'CloseRequestFcn', @closeGui);
    gameTitle = uicontrol('Units', 'pixels', 'Style', 'Text');
    gameTitle.Position = [3 * btnSz, figSize(1) + 100, btnSz * 3, btnSz * 1.5];
    gameTitle.String = 'CRS Tic Tac Toe';
    gameTitle.FontSize = 28;
    gameTitle.ForegroundColor = 'white';
    gameTitle.BackgroundColor = fig.Color;
    relPos = [figSize(1) - btnSz * 3, figSize(1) + 100, btnSz * 2, btnSz];
    toolPos = [btnSz, figSize(1) + 100, btnSz, btnSz];
    toolCPos = [2 * btnSz, figSize(1) + 100, btnSz, btnSz];
    uicontrol('Units', 'pixels', 'Style', 'pushbutton', 'String', 'Release', 'Position', relPos, 'CallBack', @releaseCB, 'FontSize', 15);
    toolBtn = uicontrol('Units', 'pixels', 'Style', 'togglebutton', 'String', 'Tool', 'Position', toolPos, 'CallBack', @toolCB);
    uicontrol('Units', 'pixels', 'Style', 'pushbutton', 'String', 'Tool Cali', 'Position', toolCPos, 'CallBack', @toolCaliCB);

    for row = 1:nRows

        for col = 1:nColumns
            strin = " ";
            color = 'k';

            if row > 2 && row < 10
                strin = [char(nRows + 1 - row + 64), num2str(col)];
                color = 'w';
            end

            uicontrol('Units', 'pixels', 'Style', 'pushbutton', 'BackgroundColor', color, 'String', strin, 'Position', btnSz * [(col), (row - 1), 1, 1], 'Callback', {@btnCB}, 'FontSize', 8);
        end

    end

    function btnCB(hObject, ~)
        % moves the robot with a push of a button
        assert(hObject.String ~= " ", "Can't play here!");
        assert(strlength(toolBtn.String) == 1, "Grip tool first!");
        assert(hObject.String ~= "F", "Already marked!");
        assert(hObject.String ~= "L", "Already marked!");
        assert(lastPlaced ~= toolBtn.String, "Change tool!");

        id = get(hObject, 'String');
        bod = getPos(id, 0, referenceX, squareSize, sedmicka);
        bodPriklep = getPos(id, 1, referenceX, squareSize, sedmicka);
        [err, cfgOuter] = getCfg(robot, bod);
        assert(~~err, 'Unreachable!');
        [err, cfgInner] = getCfg(robot, bodPriklep);
        assert(~~err, 'Unreachable!');

        hObject.FontSize = toolBtn.FontSize;
        hObject.String = toolBtn.String;

        backStep(robot, 1, referenceX, squareSize, sedmicka);

        bbmoveircs(robot, cfgOuter, 50, 5);
        bbwaitforready(robot);
        %             doklepni
        bbmoveircs(robot, cfgInner, 50, 5);
        bbwaitforready(robot);
        lastPlaced = toolBtn.String;
        %             spat
        bbmoveircs(robot, cfgOuter, 50, 5);
        bbwaitforready(robot);

        backStep(robot, 1, referenceX, squareSize, sedmicka);
        backStep(robot, 2, referenceX, squareSize, sedmicka);
    end

    function releaseCB(~, ~)
        % releases the robot with a push of a button
        bbgrip(robot, -0.09);
        bbwaitforgrip(robot);
        bbwaitforready(robot);
        bbrelease(robot);
    end

    function toolCaliCB(~, ~)
        % releases the robot with a push of a button
        toolBtn.FontSize = 10;
        toolBtn.String = "Empty";
        toolCali(robot, referenceX, squareSize, sedmicka);
    end

    function closeGui(obj, ~, ~)
        % closes program
        try
            bbgrip(robot, -0.08);
            bbwaitforgrip(robot);
            bbwaitforready(robot);
            bbrelease(robot);
        catch
            disp('Where is the robot???')
        end

        delete(obj);
        konec = input('Close comms? y/N\n', 's');
        konec = konec == 'y';

        if konec
            backStep(robot, 0, referenceX, squareSize, sedmicka);
            bbclose(robot);
        end

    end

    function toolCB(hObject, ~)
        bodL = [referenceX - 1 * squareSize, -230, 26.0295 - (5 * sedmicka), 90 * (~sedmicka), 90.0000, 0];
        bodF = [referenceX - 3 * squareSize, -230, 26.0295 - (5 * sedmicka), 90 * (~sedmicka), 90.0000, 0];
        bodLdown = [referenceX - 1 * squareSize, -230, 23.0295 - (5 * sedmicka), 90 * (~sedmicka), 90.0000, 0];
        bodFdown = [referenceX - 3 * squareSize, -230, 25.0295 - (10 * sedmicka), 90 * (~sedmicka), 90.0000, 0];
        % F dead
        % bodFdown = [referenceX - 3 * squareSize, -230, 23.0295 - (10 * sedmicka), 90 * (~sedmicka), 90.0000, 0];

        if strlength(hObject.String) > 1
            hObject.String = " ";
            gripTool(bodL, 0.9);
            hObject.String = "L";
        elseif hObject.String == "L"
            hObject.String = " ";
            gripTool(bodLdown, -0.3);
            hObject.String = "F";
            gripTool(bodF, 0.9);
        elseif hObject.String == "F"
            hObject.String = " ";
            gripTool(bodFdown, -0.3);
            hObject.String = "L";
            gripTool(bodL, 0.9);
        end

        hObject.FontSize = 20;
    end

    function gripTool(point, power)
        [err, cfgGripAbove] = getCfg(robot, point + [0, 0, 20, 0, 0, 0]);
        assert(~~err, 'Unreachable!');
        [err, cfgGrip] = getCfg(robot, point);
        assert(~~err, 'Unreachable!');
        [err, cfgGripDip] = getCfg(robot, point + [0, 0, -2, 0, 0, 0]);
        assert(~~err, 'Unreachable!');

        backStep(robot, 2, referenceX, squareSize, sedmicka);
        bbwaitforready(robot);
        bbmoveircs(robot, cfgGripAbove, 50, 5);
        bbwaitforready(robot);
        % grips tool at point
        bbmoveircs(robot, cfgGrip, 50, 5);
        bbwaitforready(robot);
        bbgrip(robot, power);
        bbwaitforgrip(robot);
        % dip in paint
        if power > 0
            bbwaitforready(robot);
            bbmoveircs(robot, cfgGripDip, 50, 5);
            bbwaitforready(robot);
            bbmoveircs(robot, cfgGrip, 50, 5);
        end

        bbmoveircs(robot, cfgGripAbove, 50, 5);
        bbwaitforready(robot);
        backStep(robot, 2, referenceX, squareSize, sedmicka);
    end

end
