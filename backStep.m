function backStep(robot, pos, referenceX, squareSize, sedmicka)
    % move arm back to avoid cylinder smash
    switch pos
        case 0
            % away
            nuDeg = bbgetdeg(robot);
            nuDeg(2) = -40;
            nuDeg(3) = 0;
            bbmovedeg(robot, nuDeg);
            bbwaitforready(robot);
        case 1
            % above board
            nuPos = getPos('E3', 0, referenceX, squareSize, sedmicka);
            nuPos(3) = nuPos(3) + 100;
            [err, cfg1] = getCfg(robot, nuPos);
            assert(~~err, 'Unreachable!');
            bbmoveircs(robot, cfg1, 50, 5);
            bbwaitforready(robot);
        case 2
            % above tools
            nuPos = [referenceX - 2 * squareSize, -230, 70.0295, 90 * (~sedmicka), 90.0000, 0];
            [err, cfg2] = getCfg(robot, nuPos);
            assert(~~err, 'Unreachable!');
            bbmoveircs(robot, cfg2, 50, 5);
            bbwaitforready(robot);
        case 3
            % right
            nuDeg = bbgetdeg(robot);
            nuDeg(1) = 40;
            nuDeg(2) = -40;
            nuDeg(3) = 0;
            bbmovedeg(robot, nuDeg);
            bbwaitforready(robot);
        case 4
            % left
            nuDeg = bbgetdeg(robot);
            nuDeg(1) = -40;
            nuDeg(2) = -40;
            nuDeg(3) = 0;
            bbmovedeg(robot, nuDeg);
            bbwaitforready(robot);
    end

end
