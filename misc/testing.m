close all;

addpath(genpath('src'))
addpath(genpath('misc'))
bodX = [581 - 4 * 38, -230, 30.0295, 0, 90.0000, 0];
bodO = [581 - 2 * 38, -230, 30.0295, 0, 90.0000, 0];
E4 = [581 + 3 * 38, 0, 200, 0.0000, 90, 0.0000];
% load('crs93.mat')
bod = getPos('J7', 1, 581, 38);
[err, cfg] = getCfg(robot, bod)

% bbcomreset();

% robot = bbopen('CRS93');
% bbresetmotors(robot);
% % robot = bbinit(robot);

% backStep(robot, 0);
% bbsofthome(robot);
% bbgrip(robot, 0.3);
% bbwaitforgrip(robot);

% [err, cfgE7F7] = bbmovex(robot, E7F7);
% assert(err == 1, 'Unreachable!');
% [err, cfgO] = bbmovex(robot, bodO);
% assert(err == 1, 'Unreachable!');
% [err, cfgX] = bbmovex(robot, bodX);
% assert(err == 1, 'Unreachable!');

% bbmoveirc(robot, cfgE7F7);
% bbwaitforready(robot);
% bbrelease(robot);
% input('Robot released, place E7F7 intersect under arm, ARM POWER, then press enter\n');

% backStep(robot, 0);

% bbgrip(robot, -0.08);
% bbwaitforgrip(robot);
% bbmoveircs(robot, cfgO, 50, 5);
% bbwaitforready(robot);

% wristRoll(robot);

% bbrelease(robot);
% input('Robot released, place O in arm, then press enter\n');

% backStep(robot, 0);

% bbmoveircs(robot, cfgX, 50, 5);
% bbwaitforready(robot);

% wristRoll(robot);

% bbrelease(robot);
% input('Robot released, place X in arm, then press enter\n');

% backStep(robot, 0);

% cB(robot)

% function cB(robot)
%     % creates gui to control robot
%     lastPlaced = " ";
%     nRows = 10;
%     nColumns = 7;
%     %     close all;
%     dispSize = get(groot, 'ScreenSize');
%     sirka = dispSize(3);
%     vyska = dispSize(4);
%     figSize = [sirka / 3, vyska - 100];
%     btnSz = figSize(1) / 9;
%     fig = figure('Color', 'black', 'pos', [(sirka - figSize(1)) / 2, (vyska - figSize(2)) / 2, figSize(1), figSize(2) - 100]);
%     set(fig, 'CloseRequestFcn', @closeGui);
%     gameTitle = uicontrol('Units', 'pixels', 'Style', 'Text');
%     gameTitle.Position = [figSize(1) / 2, figSize(1) + btnSz, btnSz * 3, btnSz * 2];
%     gameTitle.Position(1) = gameTitle.Position(1) - gameTitle.Position(3) / 2;
%     gameTitle.String = 'CRS Tic Tac Toe';
%     gameTitle.FontSize = 28;
%     gameTitle.ForegroundColor = 'white';
%     gameTitle.BackgroundColor = fig.Color;
%     relPos = [figSize(1) - btnSz * 3, figSize(1) + 100, btnSz * 2, btnSz];
%     toolPos = [btnSz, figSize(1) + 100, btnSz, btnSz];
%     uicontrol('Units', 'pixels', 'Style', 'pushbutton', 'String', 'Release', 'Position', relPos, 'CallBack', @releaseCB, 'FontSize', 15);
%     toolBtn = uicontrol('Units', 'pixels', 'Style', 'togglebutton', 'String', ' ', 'Position', toolPos, 'CallBack', @toolCB, 'FontSize', 20);

%     for row = 1:nRows

%         for col = 1:nColumns
%             strin = [char(nRows + 1 - row + 64)  num2str(col)];
%             uicontrol('Units', 'pixels', 'Style', 'pushbutton', 'String', strin, 'Position', btnSz * [(col), (row - 1), 1, 1], 'Callback', {@btnCB}, 'FontSize', 8);
%         end

%     end

%     function btnCB(hObject, ~)
%         % moves the robot with a push of a button
%         %         assert(toolBtn.String ~= " ", "Grip tool first!");
%         %         assert(hObject.String ~= "X", "Already marked!");
%         %         assert(hObject.String ~= "O", "Already marked!");
%         %         assert(lastPlaced ~= toolBtn.String, "Change tool!");
%         %
%         %

%         id = get(hObject, 'String');
%         hObject.FontSize = toolBtn.FontSize;

%         bod = getPos(id, 0);
%         bodPriklep = getPos(id, 1);

%         [err, cfg1] = bbmovex(robot, bod);
%         assert(err == 1, 'Unreachable!');

%         [err, cfg2] = bbmovex(robot, bodPriklep);
%         assert(err == 1, 'Unreachable!');

%         backStep(robot, 1);

%         bbmoveircs(robot, cfg1, 50, 5);
%         bbwaitforready(robot);
%         %             doklepni
%         bbmoveircs(robot, cfg2, 50, 5);
%         bbwaitforready(robot);
%         %             spat
%         bbmoveircs(robot, cfg1, 50, 5);
%         bbwaitforready(robot);

%         backStep(robot, 1);

%         hObject.String = toolBtn.String;
%         lastPlaced = toolBtn.String;
%     end

%     function releaseCB(~, ~)
%         % releases the robot with a push of a button
%         bbrelease(robot);
%     end

%     function closeGui(obj, ~, ~)
%         % closes program
%         bbgrip(robot, -0.4);
%         bbwaitforgrip(robot);
%         bbrelease(robot);
%         delete(obj);
%     end

%     function toolCB(hObject, ~)
%         bodL = [581 - 2 * 38, -230, 30.0295, 0, 90.0000, 0];
%         bodF = [581 - 4 * 38, -230, 30.0295, 0, 90.0000, 0];

%         if hObject.String == ' '
%             gripTool(bodL, 0.9);
%             hObject.String = "L";
%         elseif hObject.Value == 0
%             gripTool(bodL, -0.3);
%             gripTool(bodF, 0.9);
%             hObject.String = "F";
%         else
%             gripTool(bodF, -0.3);
%             gripTool(bodL, 0.9);
%             hObject.String = "L";
%         end

%         backStep(robot, 1);
%     end

%     function gripTool(point, power)
%         [err, cfgGrip2] = bbmovex(robot, point + [0, 0, 20, 0, 0, 0]);
%         assert(err == 1, 'Unreachable!');
%         bbwaitforready(robot);
%         bbmoveircs(robot, cfgGrip2, 50, 5);
%         % grips tool at point
%         [err, cfgGrip] = bbmovex(robot, point);
%         assert(err == 1, 'Unreachable!');
%         bbwaitforready(robot);
%         bbmoveircs(robot, cfgGrip, 50, 5);
%         bbwaitforready(robot);
%         bbgrip(robot, power);
%         bbwaitforgrip(robot);

%         if power > 0
%             nuDeg = bbgetdeg(robot) + [0, 0, -0.25, 0, 0, 0];
%             bbmovedeg(robot, nuDeg);
%             bbwaitforready(robot);
%         end

%         nuDeg = bbgetdeg(robot) + [0, 0, 10, 0, 0, 0];
%         bbmovedeg(robot, nuDeg);
%         bbwaitforready(robot);
%     end

% end

% for i = 1:3
%     bbmovedeg(robot, [0 -80 -9.3 0 -90 0]);
%     bbwaitforready(robot);
%     pause(0.05);
%     bbmovedeg(robot, [0 -80 0 0 -90 0]);
%     bbwaitforready(robot);
%     pause(0.05);
% end
