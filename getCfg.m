function [reachable, irc] = getCfg(rob, pos)
    % closest cfg to softhome, bbmovex inspired
    prevPos = bbdegtoirc(rob, rob.shdeg);

    angles = bbiktred(rob, pos);
    ircs = bbdegtoirc(rob, angles);

    irc = NaN(1, 6);
    reachable = ~isempty(angles);

    if ~reachable
        return
    end

    normy = vecnorm(prevPos - ircs, 2, 2);
    [~, idx] = min(normy);

    irc = ircs(idx, :);
end
