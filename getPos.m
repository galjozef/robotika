function out = getPos(id, priklep, referenceX, squareSize, sedmicka)
    % returns row vector of point location
    POLOMER = 160 - 3 * sedmicka;
    p = double(id(1)) - 64;
    c = str2double(id(2));
    assert(c > 0 && c < 8, 'Out of bounds');
    assert(p > 0 && c < 11, 'Out of bounds');
    offset = referenceX - 7 * squareSize;
    r = 185 - (sedmicka * 10) - priklep * ((25 - sedmicka * 4) + (p > 3 && p < 7) * 3);
    oneFieldAngle = squareSize * 180 / (pi * POLOMER);
    rowZeroangle = 90 - oneFieldAngle * 5;
    thetaD = p * oneFieldAngle + rowZeroangle;
    thetaR = deg2rad(thetaD);
    x = c * squareSize + offset;
    y = r * cos(thetaR);
    z = r * sin(thetaR);
    theta4 = 180 - thetaD;
    out = [x, y, z, 90, theta4, 0];
end
