close all;

addpath(genpath('src'))

sedmicka = input('CRS97? y/N\n', 's');
sedmicka = sedmicka == 'y';

REFERENCEX = 577;
SQUARESIZE = 38 + (sedmicka);
E7c = [REFERENCEX, 0, 177, 0.0000, 90, 90 * (~sedmicka)];
leftCornerDownOut = [REFERENCEX - 6 * SQUARESIZE, -250, 10, 0, 90, 88 * (~sedmicka)];
leftCornerDownPriklep = [REFERENCEX - 6 * SQUARESIZE, -170, 10, 0, 90, 88 * (~sedmicka)];
rightCornerDownOut = [REFERENCEX - 6 * SQUARESIZE, 250, 10, 0, 90, -92 * (~sedmicka)];
rightCornerDownPriklep = [REFERENCEX - 6 * SQUARESIZE, 170, 10, 0, 90, -92 * (~sedmicka)];

leftCornerUpOut = [REFERENCEX, -200, 10, 0, 90, 88 * (~sedmicka)];
leftCornerUpPriklep = [REFERENCEX, -170, 10, 0, 90, 88 * (~sedmicka)];
rightCornerUpOut = [REFERENCEX, 200, 10, 0, 90, -92 * (~sedmicka)];
rightCornerUpPriklep = [REFERENCEX, 170, 10, 0, 90, -92 * (~sedmicka)];

bbcomreset();

if sedmicka
    robot = bbopen('CRS97');
else
    robot = bbopen('CRS93');
end

hafoha = input('Hardhome? y/N\n', 's');
hafoha = hafoha == 'y';

if hafoha
    robot = bbinit(robot);
end

bbresetmotors(robot);
%%
[err, cfgE7c] = getCfg(robot, E7c);
assert(~~err, 'Unreachable!');
[err, cfglCDO] = getCfg(robot, leftCornerDownOut);
assert(~~err, 'Unreachable!');
[err, cfglCDP] = getCfg(robot, leftCornerDownPriklep);
assert(~~err, 'Unreachable!');
[err, cfgrCDO] = getCfg(robot, rightCornerDownOut);
assert(~~err, 'Unreachable!');
[err, cfgrCDP] = getCfg(robot, rightCornerDownPriklep);
assert(~~err, 'Unreachable!');

[err, cfglCUO] = getCfg(robot, leftCornerUpOut);
assert(~~err, 'Unreachable!');
[err, cfglCUP] = getCfg(robot, leftCornerUpPriklep);
assert(~~err, 'Unreachable!');
[err, cfgrCUO] = getCfg(robot, rightCornerUpOut);
assert(~~err, 'Unreachable!');
[err, cfgrCUP] = getCfg(robot, rightCornerUpPriklep);
assert(~~err, 'Unreachable!');

backStep(robot, 0, REFERENCEX, SQUARESIZE, sedmicka);
backStep(robot, 1, REFERENCEX, SQUARESIZE, sedmicka);
% bbsofthome(robot);
bbgrip(robot, -0.07);
bbwaitforgrip(robot);

bbmoveirc(robot, cfgE7c);
bbwaitforready(robot);
bbrelease(robot);
input('Robot released, place E7 under arm, ARM POWER, then press enter\n');
%%
backStep(robot, 0, REFERENCEX, SQUARESIZE, sedmicka);
backStep(robot, 4, REFERENCEX, SQUARESIZE, sedmicka);
% left corner down
bbwaitforready(robot);
bbmoveircs(robot, cfglCDO, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfglCDP, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfglCDO, 50, 5);

backStep(robot, 2, REFERENCEX, SQUARESIZE, sedmicka);

% left corner up
bbwaitforready(robot);
bbmoveircs(robot, cfglCUO, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfglCUP, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfglCUO, 50, 5);

backStep(robot, 0, REFERENCEX, SQUARESIZE, sedmicka);
backStep(robot, 3, REFERENCEX, SQUARESIZE, sedmicka);

% right corner down
bbwaitforready(robot);
bbmoveircs(robot, cfgrCDO, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfgrCDP, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfgrCDO, 50, 5);

% left corner up
bbwaitforready(robot);
bbmoveircs(robot, cfgrCUO, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfgrCUP, 50, 5);
bbwaitforready(robot);
bbmoveircs(robot, cfgrCUO, 50, 5);

backStep(robot, 0, REFERENCEX, SQUARESIZE, sedmicka);
backStep(robot, 4, REFERENCEX, SQUARESIZE, sedmicka);

%%
toolCali(robot, REFERENCEX, SQUARESIZE, sedmicka);
%%
createButtons(robot, REFERENCEX, SQUARESIZE, sedmicka);
