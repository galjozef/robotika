function toolCali(robot, referenceX, squareSize, sedmicka)
    bodFCali = [referenceX - 3 * squareSize, -230, 50.0295 - (20 * sedmicka), 90 * (~sedmicka), 90.0000, 0];
    bodFCaliUp = [referenceX - 3 * squareSize, -230, 70.0295, 90 * (~sedmicka), 90.0000, 0];
    bodLCali = [referenceX - 1 * squareSize, -230, 50.0295 - (20 * sedmicka), 90 * (~sedmicka), 90.0000, 0];
    bodLCaliUp = [referenceX - 1 * squareSize, -230, 70.0295, 90 * (~sedmicka), 90.0000, 0];

    [err, cfgL] = getCfg(robot, bodLCali);
    assert(~~err, 'Unreachable!');
    [err, cfgF] = getCfg(robot, bodFCali);
    assert(~~err, 'Unreachable!');
    [err, cfgLUp] = getCfg(robot, bodLCaliUp);
    assert(~~err, 'Unreachable!');
    [err, cfgFUp] = getCfg(robot, bodFCaliUp);
    assert(~~err, 'Unreachable!');

    backStep(robot, 2, referenceX, squareSize, sedmicka);

    bbgrip(robot, -0.3);
    bbwaitforgrip(robot);
    bbmoveircs(robot, cfgL, 50, 5);
    bbwaitforready(robot);

    bbrelease(robot);
    input('Robot released, place L in arm, ARM POWER, then press enter\n');
    bbmoveircs(robot, cfgLUp, 50, 5);
    bbwaitforready(robot);
    backStep(robot, 2, referenceX, squareSize, sedmicka);

    bbmoveircs(robot, cfgF, 50, 5);
    bbwaitforready(robot);

    bbrelease(robot);
    input('Robot released, place F in arm, ARM POWER, then press enter\n');
    bbmoveircs(robot, cfgFUp, 50, 5);
    bbwaitforready(robot);
    backStep(robot, 2, referenceX, squareSize, sedmicka);

end
