pole = {
'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7';
'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7';
'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7';
'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7';
'E1', 'E2', 'E3', 'E4', 'E5', 'E6', 'E7';
'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7';
'G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7';
'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7';
'I1', 'I2', 'I3', 'I4', 'I5', 'I6', 'I7';
'J1', 'J2', 'J3', 'J4', 'J5', 'J6', 'J7';
};
close all;
addpath(genpath('src'))
%%
bodyP = [];
body = [];

for x = 1:10

    for y = 1:7
        bod = getPos(pole{x, y}, 1, 580, 38, 1);
        bod2 = getPos(pole{x, y}, 0, 580, 38, 1);
        bodyP = [bodyP  bod(1:3).'];
        body = [body  bod2(1:3).'];
    end

end

ex = 1;

scatter3(bodyP(1, :), bodyP(2, :), bodyP(3, :), 'MarkerFaceColor', [0 .75 .75])
hold on
scatter3(body(1, :), body(2, :), body(3, :), 'MarkerFaceColor', 'r')

title('Body policok na valci')
file = 'stepResponse'
set(gcf, 'PaperUnits', 'centimeters ', 'OuterPosition', [150, 50, 1300, 850], 'PaperType', 'A4', 'PaperOrientation', 'landscape');
set(gcf, 'Position', [200, 100, 1200, 800]);
set(gca, 'FontSize', 20);
set(gcf, 'PaperPositionMode', 'Auto', 'PaperOrientation', 'Landscape')

if ex

    print(gcf, '-depsc2', strcat(file, '.eps'));

end

%%
errs = zeros(1, 600);

for x = 550:600
    err = NaN(7, 7);

    for p = 2:8

        for c = 1:7
            bod = getPos(pole{p, c}, 0, x, 38, 0);
            bodP = getPos(pole{p, c}, 1, x, 38, 0);
            err(p - 1, c) = getCfg(robot, bod) * getCfg(robot, bodP);
        end

    end

    errs(x) = all(all(err));
end

find(errs)
